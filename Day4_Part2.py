import sys
import hashlib

if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("Secret Key must be passed in")
    secret_key = sys.argv[1]

    number = 1
    while True:
        to_hash = secret_key + str(number)
        if hashlib.md5(to_hash.encode('utf-8')).hexdigest().startswith('000000'):
            print(number)
            break
        number += 1