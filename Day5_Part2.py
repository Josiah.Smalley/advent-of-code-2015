import sys
import re


def is_nice(string):
    pair_match = re.findall(r"(..).*\1", string)
    repeat_match = re.findall(r"(.).\1", string)
    return pair_match and repeat_match


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    with open(file_path, 'r') as f:
        num_nice = 0
        for line in f:
            line = line.strip()
            if is_nice(line):
                num_nice += 1
        print(num_nice)
