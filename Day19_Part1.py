import sys
import re
from collections import defaultdict

from common.FileLoader import iterate_over_file

replacement_map = defaultdict(set)
molecule_wrapper = []


def process_line(line):
    if line == '':
        return

    match_obj = re.match(r"^([a-zA-Z]+) => ([a-zA-Z]+)$", line)
    if match_obj is None:
        molecule_wrapper.append(line)
        return

    original, replacement = match_obj.groups()
    replacement_map[original].add(replacement)


if __name__ == "__main__":
    iterate_over_file(process_line)

    molecule = molecule_wrapper[0]
    uniques = set()
    mol_len = len(molecule)

    for key in replacement_map.keys():
        key_len = len(key)
        values = replacement_map[key]

        start = 0
        while start < mol_len:
            index = molecule.find(key, start)
            if index == -1:
                break
            prefix = molecule[:index]
            suffix = molecule[(index + key_len):]
            for v in values:
                uniques.add(prefix + v + suffix)
            start = index + 1

    print(len(uniques))
