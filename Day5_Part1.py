import sys


def is_nice(string):
    has_double = False
    num_vowels = 0
    last_char = ''
    for c in string:
        if (last_char + c) in ('ab', 'cd', 'pq', 'xy'):
            return False
        if c in 'aeiou':
            num_vowels += 1
        if last_char == c:
            has_double = True
        last_char = c
    return has_double and num_vowels > 2


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    with open(file_path, 'r') as f:
        num_nice = 0
        for line in f:
            line = line.strip()
            if is_nice(line):
                num_nice += 1
        print(num_nice)
