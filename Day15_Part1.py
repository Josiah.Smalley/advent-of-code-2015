import re
import sys
from functools import reduce

from common.FileLoader import iterate_over_file

ingredients = dict()


def process_ingredient(line):
    # ignoring calories in groups
    name, *values = re.match(
        "^([^:]+): capacity (.+), durability (.+), flavor (.+), texture (.+), calories .*$",
        line
    ).groups()
    ingredients[name] = list(map(int, values))


def maximize_properties(current_values, allowed_ingredients, remaining_tsp, depth=0):
    # def log(*args):
    #     print("\t"*depth, *args)

    # log(current_values, allowed_ingredients, remaining_tsp)
    if remaining_tsp == 0:
        return reduce(lambda result, value: result * (value if value > 0 else 0), current_values, 1)

    if len(allowed_ingredients) == 0:
        raise Exception("Reached impossible state!")

    ingredient = allowed_ingredients[-1]
    remaining_ingredients = allowed_ingredients[:-1]

    zipped = list(zip(current_values, ingredients[ingredient]))
    if len(remaining_ingredients) == 0:
        # need to use the last ingredient to maximum capacity
        return maximize_properties(
            tuple(map(lambda t: t[0] + remaining_tsp * t[1], zipped)),
            remaining_ingredients,
            0,
            depth + 1
        )

    return max(
        map(
            lambda tsp: maximize_properties(tuple(map(lambda t: t[0] + tsp * t[1], zipped)), remaining_ingredients,
                                            remaining_tsp - tsp, depth+1),
            range(0, remaining_tsp + 1)
        )
    )


if __name__ == "__main__":
    iterate_over_file(process_ingredient)
    print(maximize_properties((0, 0, 0, 0), list(ingredients.keys()), 100))
