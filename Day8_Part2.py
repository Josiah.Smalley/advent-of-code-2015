import sys
import re

to_escape = r"\"|\\"


def calculate_size_diff(string):
    original = len(string)
    encoded = 2 + len(string) + len(re.findall(to_escape, string))
    return encoded - original


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    answer = 0
    with open(file_path, 'r') as f:
        for line in f:
            line = line.strip()
            answer += calculate_size_diff(line)

    print(answer)
