import sys
import re

escaped_2 = r"\\(\\|\")"
escaped_hex = r"\\x[a-f0-9]{2}"


class Node:
    def __init__(self, value):
        self.parent = value
        self.rank = 0

    def __repr__(self):
        return str(self)

    def __str__(self):
        return "{parent: " + str(self.parent) + ", rank: " + str(self.rank) + "}"


class DisjointSet:
    def __init__(self):
        self.nodes = dict()

    def __str__(self):
        return str(self.nodes)

    def make_set(self, value):
        if value in self.nodes:
            return

        self.nodes[value] = Node(value)

    def find(self, value):
        parent = self.get_parent(value)
        while parent != value:
            self.set_parent(value, self.get_parent(parent))
            value = self.get_parent(value)
            parent = self.get_parent(value)
        return value

    def merge(self, x, y):
        x = self.find(x)
        y = self.find(y)

        if x == y:
            return False

        if self.get_rank(x) < self.get_rank(y):
            x, y = y, x

        self.set_parent(y, x)
        if self.get_rank(x) == self.get_rank(y):
            self.set_rank(x, self.get_rank(x) + 1)

        return True

    def get_parent(self, value):
        return self.nodes[value].parent

    def get_rank(self, value):
        return self.nodes[value].rank

    def set_parent(self, value, parent):
        self.nodes[value].parent = parent

    def set_rank(self, value, rank):
        self.nodes[value].rank = rank


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    ds = DisjointSet()
    edges = []
    edge_count = dict()
    with open(file_path, 'r') as f:
        for line in f:
            a, b, distance = re.match(r"^(\w+) to (\w+) = (\d+)$", line.strip()).groups()
            ds.make_set(a)
            ds.make_set(b)
            edge_count[a] = 0
            edge_count[b] = 0
            edges.append((int(distance), a, b))

    edges.sort(reverse=True)
    total = 0
    for distance, a, b in edges:
        if edge_count[a] == 2 or edge_count[b] == 2:
            continue
        edge_count[a] += 1
        edge_count[b] += 1
        if ds.merge(a, b):
            total += distance

    print(total)
