import sys

from common.PriorityQueue import PriorityQueue
from common.HashDict import HashDict

magicMissile = 'Magic Missile'
drain = 'Drain'
shield = 'Shield'
poison = 'Poison'
recharge = 'Recharge'

player_turn_s = 'Player Turn'
player_health_s = 'Player Health'
boss_health_s = 'Boss Health'
current_mana_s = 'Current Mana'
spent_mana_s = 'Spent Mana'

iteration = 'iteration'


class Spell(object):
    def __init__(self, cost, instant_damage=0, instant_heal=0, duration=0, effect_amount=0):
        self.cost = cost
        self.instant_damage = instant_damage
        self.instant_heal = instant_heal
        self.duration = duration
        self.effect_amount = effect_amount

    def __repr__(self):
        return str(self)

    def __str__(self):
        return str(vars(self))


spells = {
    magicMissile: Spell(53, instant_damage=4),
    drain: Spell(73, instant_damage=2, instant_heal=2),
    shield: Spell(113, duration=6, effect_amount=7),
    poison: Spell(173, duration=6, effect_amount=3),
    recharge: Spell(229, duration=5, effect_amount=101)
}

if __name__ == '__main__':
    if len(sys.argv) < 5:
        raise Exception("Input arguments should be: <player hp>, <initial mana>, <boss hp>, <boss damage>")

    hard_mode = False
    if len(sys.argv) > 5:
        hard_mode = sys.argv[5] == 'hard_mode'
    init_player_hp, init_mana, init_boss_hp, boss_damage = map(int, sys.argv[1:5])

    queue = PriorityQueue()

    initial_state = HashDict({
        player_turn_s: True,
        player_health_s: init_player_hp,
        boss_health_s: init_boss_hp,
        current_mana_s: init_mana,
        spent_mana_s: 0,
        shield: 0,
        poison: 0,
        recharge: 0,
        iteration: 0,
        'spells': tuple()
    })

    queue.add_element(initial_state, 0)
    # iterations = 0

    while not queue.is_empty():  # and iterations < 1000:
        # print("Queue size:", len(queue.entries))
        # iterations += 1
        state = queue.pop_element()

        # handle effects
        if hard_mode and state[player_turn_s]:
            state[player_health_s] -= 1
            if state[player_health_s] <= 0:
                # we have died
                continue

        if state[shield] > 0:
            state[shield] -= 1

        if state[poison] > 0:
            state[poison] -= 1
            state[boss_health_s] -= spells[poison].effect_amount

        if state[recharge] > 0:
            state[recharge] -= 1
            state[current_mana_s] += spells[recharge].effect_amount

        if state[boss_health_s] <= 0:
            print(f'Boss killed after spending {state[spent_mana_s]} mana')
            print('\tSpells cast', state['spells'])
            break

        if state[player_turn_s]:
            # print('  '*state[iteration], "Player's turn", state)
            did_cast = False
            for spell_name, spell in spells.items():
                current_mana = state[current_mana_s]
                if current_mana < spell.cost:
                    continue

                if spell.duration > 0 and state[spell_name] > 0:
                    # effect already in use
                    continue

                spent_mana = state[spent_mana_s] + spell.cost
                current_mana -= spell.cost

                boss_health = state[boss_health_s] - spell.instant_damage
                player_health = state[player_health_s] + spell.instant_heal

                new_state = HashDict(state)
                new_state[player_turn_s] = False
                new_state[player_health_s] = player_health
                new_state[boss_health_s] = boss_health
                new_state[current_mana_s] = current_mana
                new_state[spent_mana_s] = spent_mana
                if spell.duration > 0:
                    new_state[spell_name] = spell.duration
                new_state[iteration] += 1
                new_state['spells'] = new_state['spells'] + (spell_name,)

                # print('Adding new state:', new_state)
                queue.add_element(new_state, spent_mana)
                did_cast = True
            # if not did_cast:
            #     print("Could not cast any spells!")

        else:
            # print('  '*state[iteration], "Boss's turn", state)
            player_health = state[player_health_s]
            damage_taken = boss_damage
            if state[shield] > 0:
                damage_taken = max(1, damage_taken - spells[shield].effect_amount)
            # print(f'Taking {damage_taken} damage!', state[shield], 'Shield active' if state[shield] > 0 else 'Shield off', spells[shield].effect_amount)
            player_health -= damage_taken

            if player_health <= 0:
                continue

            new_state = HashDict(state)
            new_state[player_turn_s] = True
            new_state[player_health_s] = player_health
            new_state[iteration] += 1

            # print('Adding new state:', new_state)
            queue.add_element(new_state, new_state[spent_mana_s])
