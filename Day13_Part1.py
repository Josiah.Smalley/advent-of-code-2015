import re
from collections import defaultdict
from itertools import combinations

from common.FileLoader import map_file_lines


def process_line(line):
    person1, direction, units, person2 = re.match(
        r"^(\w+) would (gain|lose) (\d+) happiness units by sitting next to (\w+)\.",
        line
    ).groups()

    units = int(units)
    if direction == 'lose':
        units = -units

    return gen_pair(person1, person2), units


def gen_pair(person1, person2):
    if person1 < person2:
        return person1, person2
    return person2, person1


if __name__ == "__main__":
    edges = defaultdict(lambda: defaultdict(int))

    for people, weight in map_file_lines(process_line):
        p1, p2 = people
        edges[p1][p2] += weight
        edges[p2][p1] += weight

    print(edges)
    # wlog we can fix the first element of the array and can also perform slight optimizations
    # since arrays that differ by rotation/reflection result in the same arrangement.
    # That is, [a, b, c, d] is the same as [c, d, a, b] (rotate by 2)
    # and [a, b, c, d] is the same as [d, c, b, a] (reflection)
    all_people = set(edges.keys())
    first_person = all_people.pop()

    max_happiness = -float('inf')


    def calculate_max_happiness(available_people, person_on_left, person_on_right, depth=0):
        def log(*args):
            print("\t" * depth, *args)

        log(available_people, person_on_left, person_on_right)
        if len(available_people) == 0:
            log("No remaining people. Linking left and right: ", person_on_left, person_on_right)
            return edges[person_on_left][person_on_right]
        if len(available_people) == 1:
            that_person = available_people.copy().pop()
            log("One remaining person. Linking them to left and right:", person_on_left, that_person, person_on_right)
            return edges[person_on_left][that_person] + edges[that_person][person_on_right]

        max_subset_happiness = -float('inf')

        for x, y in combinations(available_people, 2):
            subset = available_people.difference({x, y})
            x_on_left = edges[person_on_left][x] + edges[person_on_right][y] + calculate_max_happiness(subset, x, y,
                                                                                                       depth + 1)
            x_on_right = edges[person_on_left][y] + edges[person_on_right][x] + calculate_max_happiness(subset, y, x,
                                                                                                        depth + 1)
            if x_on_left > x_on_right:
                log("Trying to place " + person_on_left + " next to " + x + " and " + y + " next to " + person_on_right)
                best_combo = x_on_left
            else:
                log("Trying to place " + person_on_left + " next to " + y + " and " + x + " next to " + person_on_right)
                best_combo = x_on_right

            if best_combo > max_subset_happiness:
                log("New max subset happiness found: ", best_combo)
                max_subset_happiness = best_combo

        return max_subset_happiness


    print(calculate_max_happiness(all_people, first_person, first_person))
