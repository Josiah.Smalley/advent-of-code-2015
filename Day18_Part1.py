import sys
from itertools import combinations

from common.FileLoader import map_file_lines


def process_line(line):
    return [0] + [1 if c == '#' else 0 for c in line] + [0]


# def print_grid(g):
#     for r in g[1:-1]:
#         print(''.join(map(lambda t: '#' if t else '.', r[1:-1])))
#     print("")


if __name__ == "__main__":
    if len(sys.argv) < 3:
        raise Exception("Number of steps must be passed as the second argument")
    num_steps = int(sys.argv[2])

    grid = map_file_lines(process_line)
    grid = [[0 for _ in range(len(grid[0]))]] + grid + [[0 for _ in range(len(grid[0]))]]

    grid_size = len(grid) - 2

    for _ in range(num_steps):

        new_grid = [[0 for i in range(grid_size + 2)] for j in range(grid_size + 2)]

        for i in range(1, grid_size + 1):
            for j in range(1, grid_size + 1):
                neighbors_on = 0
                for di in range(-1, 2):
                    for dj in range(-1, 2):
                        if di == 0 and dj == 0:
                            continue
                        neighbors_on += grid[i + di][j + dj]
                if (grid[i][j] == 1 and neighbors_on in (2, 3)) or (grid[i][j] == 0 and neighbors_on == 3):
                    new_grid[i][j] = 1

        grid = new_grid

    print(sum(map(lambda grid_row: len([x for x in grid_row if x]), grid)))
