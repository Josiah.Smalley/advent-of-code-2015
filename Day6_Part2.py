import sys
import re

if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    grid = [[0 for _ in range(1000)] for __ in range(1000)]

    with open(file_path, 'r') as f:
        for line in f:
            x1, y1, x2, y2 = map(int, re.findall(r"\d+", line))
            x1, x2 = sorted([x1, x2])
            y1, y2 = sorted([y1, y2])

            flag = -1 if line.startswith("turn off") else (1 if line.startswith("turn on") else 2)

            for x in range(x1, x2 + 1):
                for y in range(y1, y2 + 1):
                    grid[x][y] = max(0, grid[x][y] + flag)

    print(sum(map(sum, grid)))
