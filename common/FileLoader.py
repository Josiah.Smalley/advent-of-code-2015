import sys


def iterate_over_file(line_action, strip_line=True):
    with open(get_file_path(), 'r') as f:
        for line in f:
            line_action(line if not strip_line else line.strip())


def iterate_over_file_get_first(line_action, strip_line=True):
    with open(get_file_path(), 'r') as f:
        for line in f:
            result = line_action(line if not strip_line else line.strip())
            if result is not None:
                return result


def map_file_lines(line_map, strip_line=True):
    with open(get_file_path(), 'r') as f:
        return [line_map(line if not strip_line else line.strip()) for line in f]


def get_file_path():
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]
    if not file_path:
        raise Exception("File path must be defined")
    return file_path
