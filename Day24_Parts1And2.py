import sys

from common.FileLoader import map_file_lines
from itertools import combinations
from functools import reduce

def find_solution(gifts, num_partitions):
    partition_weight = sum(gifts) // num_partitions
    num_gifts = len(gifts)
    valid_partitions = []

    for partition_size in range(1, num_gifts):
        valid_partitions = list(filter(lambda part: sum(part) == partition_weight, combinations(gifts, partition_size)))
        if len(valid_partitions) > 0:
            break

    return sorted(map(lambda part: (reduce(lambda agg, curr: agg * curr, part, 1), part), valid_partitions))[0]


if __name__ == '__main__':
    if len(sys.argv) < 3:
        raise Exception("Arguments must be: <file path>, <number of groups>")
    num_groups = int(sys.argv[2])
    all_gifts = list(sorted(map_file_lines(int), reverse=True))
    print(find_solution(all_gifts, num_groups))
