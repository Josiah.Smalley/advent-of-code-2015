import re
import sys
from collections import defaultdict
from itertools import combinations

from common.FileLoader import map_file_lines


def process_reindeer(time, line):
    name, velocity, flying, resting = re.match(r"^(\w+).*?(\d+).*?(\d+).*?(\d+)", line).groups()
    print(name, velocity, flying, resting)
    return distance_travelled(time, int(velocity), int(flying), int(resting)), name


def distance_travelled(time, velocity, flying, resting):
    period_length = flying + resting
    return velocity * (flying * (time // period_length) + min(time % period_length, flying))


if __name__ == "__main__":
    if len(sys.argv) < 3:
        raise Exception("The number of seconds flown must be passed in as the second parameter")

    race_duration = int(sys.argv[2])
    distances = map_file_lines(lambda line: process_reindeer(race_duration, line))
    print(sorted(distances, reverse=True)[0])
