import re
import json
import sys
from functools import reduce

from common.FileLoader import iterate_over_file_get_first

known_data = {"children": 3,
              "cats": 7,
              "samoyeds": 2,
              "pomeranians": 3,
              "akitas": 0,
              "vizslas": 0,
              "goldfish": 5,
              "trees": 3,
              "cars": 2,
              "perfumes": 1}


def process_aunt(line):
    index, data = re.match(r"Sue (\d+): (.*)$", line).groups()
    data = json.loads("{" + re.sub(r"([a-z]+)", r'"\1"', data) + "}")
    for key in ["cats", "trees"]:
        if key in data and data[key] <= known_data[key]:
            return None
    for key in ["pomeranians", "goldfish"]:
        if key in data and data[key] >= known_data[key]:
            return None

    for key in ["children", "cats", "samoyeds", "akitas", "vizslas", "cars", "perfumes"]:
        if key in data and data[key] != known_data[key]:
            return None

    return index


if __name__ == "__main__":
    print(iterate_over_file_get_first(process_aunt))
