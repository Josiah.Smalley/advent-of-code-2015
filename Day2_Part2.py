import sys

if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    with open(file_path, 'r') as f:
        total = 0
        for line in f:
            a, b, c = sorted(map(int, line.split('x')))
            total += 2*(a+b) + a*b*c
        print(total)
