import sys
from itertools import combinations

from common.FileLoader import map_file_lines

if __name__ == "__main__":
    if len(sys.argv) < 3:
        raise Exception("Amount of eggnog must be passed as the second argument")

    liters = int(sys.argv[2])
    containers = list(map_file_lines(int))
    num_combos = 0
    for size in range(1, len(containers)):
        for c in combinations(containers, size):
            if sum(c) == liters:
                num_combos += 1
        if num_combos > 0:
            print(num_combos)
            break
