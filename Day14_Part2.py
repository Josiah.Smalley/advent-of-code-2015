import re
import sys
from collections import defaultdict
from itertools import combinations

from common.FileLoader import map_file_lines


class Reindeer:
    def __init__(self, name, velocity, flying, resting):
        self.name = name
        self.velocity = velocity
        self.flying = flying
        self.period = flying + resting
        self.position = 0
        self.time = 0
        self.points = 0

    def fly(self):
        if self.time < self.flying:
            self.position += self.velocity
        self.time = (self.time + 1) % self.period

    def add_point(self):
        self.points += 1

    def __repr__(self):
        return str({"name": self.name, "position": self.position, "points": self.points})


def process_reindeer(line):
    name, velocity, flying, resting = re.match(r"^(\w+).*?(\d+).*?(\d+).*?(\d+)", line).groups()
    return Reindeer(name, int(velocity), int(flying), int(resting))


if __name__ == "__main__":
    if len(sys.argv) < 3:
        raise Exception("The number of seconds flown must be passed in as the second parameter")

    race_duration = int(sys.argv[2])
    reindeer = map_file_lines(process_reindeer)

    for t in range(race_duration):
        max_distance = 0
        winning_reindeer = set()
        for r in reindeer:
            r.fly()
            if r.position >= max_distance:
                if r.position > max_distance:
                    winning_reindeer.clear()
                max_distance = r.position
                winning_reindeer.add(r)
        for r in winning_reindeer:
            r.add_point()

    print(max(map(lambda x: x.points, reindeer)))
