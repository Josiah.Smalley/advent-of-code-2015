import sys
import re

if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")


    def resolve(node):
        print('Resolving: ' + node)
        stack = [node]

        while len(stack) > 0:
            current = stack.pop()
            if current in values:
                continue
            print("resolving: " + current)
            children_to_add = []
            for c in depends_on[current]:
                if c not in depends_on:
                    return 'ERROR: ' + c + ' not found'
                if c not in values:
                    children_to_add.append(c)

            if len(children_to_add) != 0:
                stack.append(current)
                for c in children_to_add:
                    stack.append(c)
            else:
                values[current] = calculate(current)

        return values[node]


    def calculate(node):
        c = calculations[node]
        if c[0] == 'assigment':
            return resolve_source(c[1])
        if c[0] == 'and':
            return resolve_source(c[1]) & resolve_source(c[2])
        if c[0] == 'or':
            return resolve_source(c[1]) | resolve_source(c[2])
        if c[0] == 'not':
            return ~resolve_source(c[1]) & (2 ** 16 - 1)
        if c[0] == 'l':
            return (resolve_source(c[1]) << c[2]) & (2 ** 16 - 1)
        if c[0] == 'r':
            return resolve_source(c[1]) >> c[2]

        return None


    def resolve_source(s):
        if s.isnumeric():
            return int(s)
        return values[s]


    values = dict()
    depends_on = dict()
    calculations = dict()
    assignment_pattern = r"^([a-z0-9]+) -> ([a-z]+)$"
    andor_pattern = r"^([a-z0-9]+) (OR|AND) ([a-z0-9]+) -> ([a-z]+)$"
    not_pattern = r"^NOT ([a-z0-9]+) -> ([a-z]+)$"
    shift_pattern = r"^([a-z0-9]+) (R|L)SHIFT (\d+) -> ([a-z]+)$"

    with open(file_path, 'r') as f:
        for line in f:
            assignment = re.match(assignment_pattern, line)
            if assignment:
                source, target = assignment.groups()
                depends_on[target] = set() if source.isnumeric() else {source}
                calculations[target] = ('assigment', source)
                continue

            andor_match = re.match(andor_pattern, line)
            if andor_match:
                first, op, second, target = andor_match.groups()
                children = set()
                if not first.isnumeric():
                    children.add(first)
                if not second.isnumeric():
                    children.add(second)
                depends_on[target] = children
                calculations[target] = (op.lower(), first, second)
                continue

            not_match = re.match(not_pattern, line)
            if not_match:
                source, target = not_match.groups()
                depends_on[target] = set() if source.isnumeric() else {source}
                calculations[target] = ('not', source)
                continue

            shift_match = re.match(shift_pattern, line)
            if shift_match:
                source, shift, value, target = shift_match.groups()
                depends_on[target] = set() if source.isnumeric() else {source}
                calculations[target] = (shift.lower(), source, int(value))
                continue

    print(resolve('a'))
