import sys
from math import floor


def calculate_gifts(house_number):
    if house_number == 1:
        return 10
    total_gifts = 0
    for i in range(1, 1 + floor(house_number ** 0.5)):
        if house_number % i == 0:
            total_gifts += i * 10
            j = house_number // i
            if j != i:
                total_gifts += j * 10
    return total_gifts


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("The first parameter must be the minimum bound")

    min_bound = int(sys.argv[1])
    gifts = 10
    house = 1
    while gifts < min_bound:
        house += 1
        gifts = calculate_gifts(house)

    # house //= 32
    # gifts = calculate_gifts(gifts)
    # while gifts < min_bound:
    #     house += 1
    #     gifts = calculate_gifts(house)

    print("RESULT:", house, gifts)
