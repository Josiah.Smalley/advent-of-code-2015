import sys


def find_code_index(row, col):
    diag = row + col - 2
    diag_start = (diag * (diag + 1)) // 2 + 1
    row_diff = diag - row + 1
    return diag_start + row_diff


def increment_code(value):
    return (value * 252533) % 33554393


if __name__ == '__main__':
    if len(sys.argv) < 3:
        raise Exception("Arguments should be: <code row>, <code col>")

    code_row, code_col = map(int, sys.argv[1:3])
    code = 20151125
    for _ in range(find_code_index(code_row, code_col) - 1):
        code = increment_code(code)
    print(code)
