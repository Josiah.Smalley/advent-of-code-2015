import sys
import json
import re


def recursive_sum(obj):
    the_sum = 0
    object_type = type(obj)
    if object_type is list:
        for entry in obj:
            entry_type = type(entry)
            if entry_type == int:
                the_sum += entry
            elif entry_type == list or entry_type == dict:
                the_sum += recursive_sum(entry)
    elif object_type is dict:
        sub_sum = 0
        for key, value in obj.items():
            value_type = type(value)
            if value_type == str and value == 'red':
                sub_sum = 0
                break
            elif value_type == int:
                sub_sum += value
            elif value_type == list or value_type == dict:
                sub_sum += recursive_sum(value)
        the_sum += sub_sum
    return the_sum


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    total = 0
    with open(file_path, 'r') as f:
        for line in f:
            total += recursive_sum(json.loads(line))

    print(total)
