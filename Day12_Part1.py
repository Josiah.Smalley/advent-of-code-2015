import sys
import re

if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    total = 0
    with open(file_path, 'r') as f:
        for line in f:
            total += sum(map(int, re.findall(r"-?\d+", line)))

    print(total)
