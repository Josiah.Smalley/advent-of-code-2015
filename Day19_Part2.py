import re
from random import shuffle

from common.FileLoader import iterate_over_file

replacements = []
molecule_wrapper = []


def process_line(line):
    if line == '':
        return

    match_obj = re.match(r"^([a-zA-Z]+) => ([a-zA-Z]+)$", line)
    if match_obj is None:
        molecule_wrapper.append(line)
        return

    original, replacement = match_obj.groups()
    replacements.append((original, replacement))



if __name__ == "__main__":
    iterate_over_file(process_line)

    molecule = molecule_wrapper[0]

    target = molecule
    num_steps = 0

    while target != 'e':
        temp = target
        for orig, repl in replacements:
            if repl not in target:
                continue

            target = target.replace(repl, orig, 1)
            num_steps += 1

        if target == temp:
            # nothing changed. Try again
            target = molecule
            num_steps = 0
            shuffle(replacements)

    print(num_steps)
