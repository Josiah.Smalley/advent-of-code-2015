import math
import sys
from itertools import product
from collections import defaultdict

weapons = [
    (8, 4, 0),
    (10, 5, 0),
    (25, 6, 0),
    (40, 7, 0),
    (74, 8, 0)
]

armor = [
    (13, 0, 1),
    (31, 0, 2),
    (53, 0, 3),
    (75, 0, 4),
    (102, 0, 5)
]

rings = [
    (25, 1, 0),
    (50, 2, 0),
    (100, 3, 0),
    (20, 0, 1),
    (40, 0, 2),
    (80, 0, 3)
]

weapon_options = {(dam, dfn): c for c, dam, dfn in weapons}
armor_options = {(dam, dfn): c for c, dam, dfn in armor}  # accounts for 1 armor
armor_options[(0, 0)] = 0  # accounts for "no armor"

ring_options = {(dam, dfn): c for c, dam, dfn in rings}  # accounts for exactly 1 ring
ring_options[(0, 0)] = 0  # accounts for "no rings"
# accounts for exactly 2 rings
for i in range(len(rings)):
    for j in range(i + 1, len(rings)):
        iCost, iDam, iDef = rings[i]
        jCost, jDam, jDef = rings[j]
        stats = (iDam + jDam, iDef + jDef)
        if stats in ring_options:
            ring_options[stats] = max(iCost + jCost, ring_options[stats])
        else:
            ring_options[stats] = iCost + jCost

all_options = defaultdict(lambda: 0)


def add_option(cost, damage, defense):
    key = (damage, defense)
    all_options[key] = max(cost, all_options[key])


for wKey, aKey, rKey in product(weapon_options, armor_options, ring_options):
    totalDamage, totalDefense = tuple(map(sum, zip(wKey, aKey, rKey)))
    totalCost = weapon_options[wKey] + armor_options[aKey] + ring_options[rKey]
    add_option(totalCost, totalDamage, totalDefense)

option_list = tuple(sorted([(all_options[(a, d)], a, d) for a, d in all_options], reverse=True))


def turns_to_death(defender_health, defender_armor, attacker_damage):
    return math.ceil(defender_health / max(1, attacker_damage - defender_armor))


if __name__ == "__main__":
    if len(sys.argv) < 5:
        raise Exception("Input parameters must be: [player health], [boss health], [boss damage], [boss armor]")

    player_health, boss_health, boss_damage, boss_armor = map(int, sys.argv[1:5])

    for cost, player_damage, player_armor in option_list:
        turns_to_player_death = turns_to_death(player_health, player_armor, boss_damage)
        turns_to_boss_death = turns_to_death(boss_health, boss_armor, player_damage)
        if turns_to_player_death <= turns_to_boss_death:
            print('Cost of:', cost, 'with (damage, armor)=', (player_damage, player_armor))
            print('\t', turns_to_player_death, turns_to_boss_death)
            break
