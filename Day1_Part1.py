import sys

if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    with open(file_path, 'r') as f:
        print(sum(map(lambda x: 1 if x == '(' else -1, f.readline().strip())))
