import re
import sys
from functools import reduce

from common.FileLoader import iterate_over_file

ingredients = dict()


def process_ingredient(line):
    name, *values = re.match(
        "^([^:]+): capacity (.+), durability (.+), flavor (.+), texture (.+), calories (.*)$",
        line
    ).groups()
    values = list(map(int, values))
    ingredients[name] = (values[:-1], values[-1])


def maximize_properties(current_values, allowed_ingredients, remaining_tsp, remaining_calories, depth=0):
    # def log(*args):
    #     print("\t"*depth, *args)

    # log(current_values, allowed_ingredients, remaining_tsp)
    if remaining_tsp == 0:
        if remaining_calories == 0:
            return reduce(lambda result, value: result * (value if value > 0 else 0), current_values, 1)
        return 0

    if len(allowed_ingredients) == 0:
        raise Exception("Reached impossible state!")

    ingredient = allowed_ingredients[-1]
    remaining_ingredients = allowed_ingredients[:-1]

    zipped = list(zip(current_values, ingredients[ingredient][0]))
    if len(remaining_ingredients) == 0:
        # need to use the last ingredient to maximum capacity
        if ingredients[ingredient][1] * remaining_tsp != remaining_calories:
            return 0
        return maximize_properties(
            tuple(map(lambda t: t[0] + remaining_tsp * t[1], zipped)),
            remaining_ingredients,
            0,
            0,
            depth + 1
        )

    return max(
        map(
            lambda tsp: maximize_properties(tuple(map(lambda t: t[0] + tsp * t[1], zipped)),
                                            remaining_ingredients,
                                            remaining_tsp - tsp,
                                            remaining_calories - tsp*ingredients[ingredient][1],
                                            depth+1),
            range(0, remaining_tsp + 1)
        )
    )


if __name__ == "__main__":
    iterate_over_file(process_ingredient)
    print(maximize_properties((0, 0, 0, 0), list(ingredients.keys()), 100, 500))
