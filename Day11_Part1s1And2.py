import sys
import re

escaped_2 = r"\\(\\|\")"
escaped_hex = r"\\x[a-f0-9]{2}"


def has_straight(arr):
    for i in range(1, 7):
        if arr[i - 1] + 1 == arr[i] and arr[i] + 1 == arr[i + 1]:
            return True
    return False


def has_pairs(arr):
    i = 0
    found_first = False
    while i < 7:
        if arr[i] == arr[i + 1]:
            if found_first:
                return True
            found_first = True
            i += 1
        i += 1
    return False


def increment(arr):
    i = 7
    while i >= 0:
        arr[i] += 1
        if arr[i] < 26:
            break
        arr[i] = 0
        i -= 1

    i = 0
    while i < 8:
        if arr[i] in (8, 11, 14):
            arr[i] += 1
            i += 1
            while i < 8:
                arr[i] = 0
                i += 1
        i += 1
    return arr


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("Santa's Current password must be defined")
    current_password = sys.argv[1]
    a = ord('a')

    password = list(map(lambda c: ord(c) - a, current_password))
    pass_string = current_password
    while True:
        increment(password)
        # pass_string = ''.join(map(lambda c: chr(c + a), password))
        if not has_straight(password):
            # print("Does not contain straight", pass_string)
            continue
        if not has_pairs(password):
            # print("Does not contain pairs", pass_string)
            continue

        break
    pass_string = ''.join(map(lambda c: chr(c + a), password))
    print(pass_string)
