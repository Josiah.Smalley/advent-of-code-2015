import sys
import re



if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("Start value must be passed in")
    if len(sys.argv) < 3:
        raise Exception("Number of iterations must be passed in")
    value = sys.argv[1]
    num_iterations = int(sys.argv[2])

    for _ in range(num_iterations):
        value = re.sub(
            r"((.)\2*)",
            lambda match: str(len(match.group(1))) + match.group(2),
            value
        )

    print(len(value))