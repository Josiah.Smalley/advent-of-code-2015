import sys

if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    with open(file_path, 'r') as f:
        seen = set()
        positions = [[0, 0], [0, 0]]
        seen.add((0, 0))
        index = 0
        for c in f.readline().strip():
            pos = positions[index % 2]
            if c == '^':
                pos[1] += 1
            elif c == 'v':
                pos[1] -= 1
            elif c == '<':
                pos[0] -= 1
            elif c == '>':
                pos[0] += 1
            seen.add(tuple(pos))
            index += 1
        print(len(seen))
