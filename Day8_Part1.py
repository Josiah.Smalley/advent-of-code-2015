import sys
import re

escaped_2 = r"\\(\\|\")"
escaped_hex = r"\\x[a-f0-9]{2}"


def calculate_size_diff(string):
    result = len(string)
    string = string[1:-1]
    memory = len(string) - len(re.findall(escaped_2, string)) - 3 * len(re.findall(escaped_hex, string))
    return result - memory


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    answer = 0
    with open(file_path, 'r') as f:
        for line in f:
            line = line.strip()
            answer += calculate_size_diff(line)

    print(answer)
