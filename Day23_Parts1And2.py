import sys

from common.FileLoader import map_file_lines
import re


class Computer(object):
    def __init__(self, initial_registers):
        self.registers = initial_registers
        self.operations = {
            'hlf': self.half,
            'tpl': self.triple,
            'inc': self.increment,
            'jmp': self.jump,
            'jie': self.jump_if_even,
            'jio': self.jump_if_one
        }

    def get_register(self, register):
        return self.registers[register]

    def half(self, register):
        self.registers[register] //= 2
        return 1

    def triple(self, register):
        self.registers[register] *= 3
        return 1

    def increment(self, register):
        self.registers[register] += 1
        return 1

    def jump(self, offset):
        return offset

    def jump_if_even(self, register, offset):
        if self.registers[register] % 2 == 0:
            return offset
        return 1

    def jump_if_one(self, register, offset):
        if self.registers[register] == 1:
            return offset
        return 1


reg_op_pattern = r"^([a-z]{3}) ([ab])$"
jmp_op_pattern = r"^(jmp) ([+-])(\d+)$"
logic_op_pattern = r"^([a-z]{3}) ([ab]), ([+-])(\d+)$"


def process_line(line):
    match = re.match(reg_op_pattern, line)
    if match:
        return match.groups()
    match = re.match(jmp_op_pattern, line)
    if match:
        op, sign, value = match.groups()
        return op, (1 if sign == '+' else -1) * int(value)
    match = re.match(logic_op_pattern, line)
    if match:
        op, register, sign, value = match.groups()
        return op, register, (1 if sign == '+' else '-1') * int(value)
    return line


if __name__ == '__main__':
    instructions = map_file_lines(process_line)
    index = 0
    max_instruction = len(instructions)

    init_a = 0
    if len(sys.argv) > 2:
        init_a = int(sys.argv[2])

    computer = Computer({'a': init_a, 'b': 0})
    while 0 <= index < max_instruction:
        op, *args = instructions[index]
        index += computer.operations[op](*args)

    print(f'Register A: {computer.get_register("a")}; Register B: {computer.get_register("b")}')
